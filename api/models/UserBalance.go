package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

type UserBalance struct {
	ID        uint32    `gorm:"primary_key;auto_increment" json:"id"`
	UserID    uint32    `sql:"type:int REFERENCES banks(id)" json:"user_id"`
	Balance   uint32    `json:"balance"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (u *UserBalance) Prepare() {
	u.ID = 0
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

func (p *UserBalance) FindUserBalance(db *gorm.DB, uid uint32) (*UserBalance, error) {
	var err error
	err = db.Debug().Model(&UserBalance{}).Where("user_id = ?", uid).Take(&p).Error
	if err != nil {
		return &UserBalance{}, err
	}
	return p, nil
}
