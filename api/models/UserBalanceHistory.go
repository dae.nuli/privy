package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// model define

type UserBalanceHistory struct {
	ID            uint32 `gorm:"primary_key;auto_increment" json:"id"`
	UserID        uint32 `sql:"type:int REFERENCES users(id)" json:"user_id"`
	BalanceBefore uint32 `json:"balance_before"`
	BalanceAfter  uint32 `json:"balance_after"`
	// Activity      string `gorm:"size:100;default:null"`
	// Type string `gorm:"size:10;default:null" json:"type"`
	Type Type `json:"type" sql:"type:ENUM('debit', 'kredit')"`
	// IP            string    `gorm:"size:10;default:null"`
	// Location      string    `gorm:"size:100;default:null"`
	// UserAgent     string    `gorm:"size:100;default:null"`
	// Author        string    `gorm:"size:100;default:null"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (u *UserBalanceHistory) Prepare() {
	u.ID = 0
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

func (u *UserBalanceHistory) Create(db *gorm.DB) (*UserBalanceHistory, error) {

	var err error
	err = db.Debug().Create(&u).Error
	if err != nil {
		return &UserBalanceHistory{}, err
	}
	return u, nil
}
