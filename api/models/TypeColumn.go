package models

import "database/sql/driver"

type Type string

const (
	Debit  Type = "debit"
	Kredit Type = "kredit"
)

func (e *Type) Scan(value interface{}) error {
	*e = Type(value.([]byte))
	return nil
}

func (e Type) Value() (driver.Value, error) {
	return string(e), nil
}
