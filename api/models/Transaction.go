package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

type Transaction struct {
	ID uint32 `gorm:"primary_key;auto_increment" json:"id"`
	// FromUser   User      `json:"from_user"`
	FromUserID uint32 `sql:"type:int REFERENCES users(id)" json:"from_user_id"`
	// ToUser     User      `json:"to_user"`
	ToUserID  uint32    `sql:"type:int REFERENCES users(id)" json:"to_user_id"`
	Amount    uint32    `json:"amount"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (p *Transaction) Prepare() {
	p.ID = 0
	// p.FromUser = User{}
	// p.ToUser = User{}
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()
}

type TransactionRequest struct {
	AccountNumber uint32 `json:"account_number"`
	Amount        uint32 `json:"amount"`
	PIN           uint32 `json:"pin"`
}

func (p *TransactionRequest) Validate() error {

	if p.AccountNumber < 1 {
		return errors.New("Nomor Rekening tidak boleh kosong")
	}
	if p.Amount < 1 {
		return errors.New("Jumlah Uang yang akan ditransfer tidak boleh kosong")
	}
	if p.PIN < 1 {
		return errors.New("Nomor PIN tidak boleh kosong")
	}
	return nil
}

func (u *Transaction) Create(db *gorm.DB, senderID uint32, receiverID uint32, amount uint32) (*Transaction, error) {

	// var err error
	transaction := Transaction{
		FromUserID: senderID,
		ToUserID:   receiverID,
		Amount:     amount,
	}
	db.Create(&transaction)
	// if err != nil {
	// 	return &Transaction{}, err
	// }
	var sender UserBalance
	db.Where("user_id = ?", senderID).First(&sender)

	var receiver UserBalance
	db.Where("user_id = ?", receiverID).First(&receiver)

	var historySender = UserBalanceHistory{
		UserID:        senderID,
		BalanceBefore: sender.Balance,
		BalanceAfter:  (sender.Balance - amount),
		Type:          "kredit",
	}
	db.Create(&historySender)
	var historyReceiver = UserBalanceHistory{
		UserID:        receiverID,
		BalanceBefore: receiver.Balance,
		BalanceAfter:  (receiver.Balance + amount),
		Type:          "debit",
	}
	db.Create(&historyReceiver)

	db.Model(&sender).Update("balance", sender.Balance-amount)
	db.Model(&receiver).Update("balance", receiver.Balance+amount)

	return &transaction, nil
}

func (p *Transaction) GetAll(db *gorm.DB) (*[]Transaction, error) {
	var err error
	data := []Transaction{}
	err = db.Debug().Model(&Transaction{}).Limit(100).Find(&data).Error
	if err != nil {
		return &[]Transaction{}, err
	}
	return &data, nil
}
