package controllers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/daenuli/privy/api/auth"
	"github.com/daenuli/privy/api/models"
	"github.com/daenuli/privy/api/responses"
	"github.com/daenuli/privy/api/utils/formaterror"
)

func (server *Server) GetTransactionList(w http.ResponseWriter, r *http.Request) {
	transaction := models.Transaction{}

	data, err := transaction.GetAll(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, data)
}

func (server *Server) CreateTransaction(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	transaction := models.Transaction{}
	balance := models.UserBalance{}
	transactionRequest := models.TransactionRequest{}
	err = json.Unmarshal(body, &transactionRequest)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	transaction.Prepare()
	err = transactionRequest.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	userReceive := models.User{}
	dataReceiver, err := userReceive.FindUserByAccountNumber(server.DB, uint32(uid), transactionRequest.AccountNumber)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	userSender := models.User{}
	dataSender, err := userSender.FindUserByPIN(server.DB, uint32(uid), transactionRequest.PIN)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	userBalance, err := balance.FindUserBalance(server.DB, uint32(uid))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Check if the balance enough
	if transactionRequest.Amount > userBalance.Balance {
		responses.ERROR(w, http.StatusBadRequest, errors.New("Saldo anda tidak cukup"))
		return
	}

	transactionCreated, err := transaction.Create(server.DB, dataSender.ID, dataReceiver.ID, transactionRequest.Amount)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	// w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, transactionCreated.ID))
	responses.JSON(w, http.StatusCreated, transactionCreated)
}
