package controllers

import (
	"net/http"

	"github.com/daenuli/privy/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Welcome To Privy API")
}
