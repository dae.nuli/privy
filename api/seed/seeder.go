package seed

import (
	"log"

	"github.com/daenuli/privy/api/models"
	"github.com/jinzhu/gorm"
)

// var banks = []models.Bank{
// 	models.Bank{
// 		Name: "Bank Negara Indonesia",
// 	},
// 	models.Bank{
// 		Name: "Bank Rakyat Indonesia",
// 	},
// 	models.Bank{
// 		Name: "Bank Tabungan Negara",
// 	},
// 	models.Bank{
// 		Name: "Bank Mandiri",
// 	},
// }

var users = []models.User{
	{
		Username:      "John Doe",
		Email:         "john@gmail.com",
		Password:      "password",
		Status:        1,
		AccountNumber: 11111111,
		PIN:           112233,
	},
	{
		Username:      "Lionel Messi",
		Email:         "messi@gmail.com",
		Password:      "password",
		Status:        1,
		AccountNumber: 11111112,
		PIN:           113344,
	},
	{
		Username:      "Amber Heard",
		Email:         "amber@gmail.com",
		Password:      "password",
		Status:        0,
		AccountNumber: 11111113,
		PIN:           114455,
	},
}

var balance = []models.UserBalance{
	{
		Balance: 5000000,
	},
	{
		Balance: 10000000,
	},
	{
		Balance: 9000000,
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.UserBalance{}, &models.UserBalanceHistory{}, &models.User{}, &models.Transaction{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}
	err = db.Debug().AutoMigrate(&models.User{}, &models.UserBalance{}, &models.UserBalanceHistory{}, &models.Transaction{}).Error
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	for i, _ := range users {

		// err = db.Debug().Model(&models.Bank{}).Create(&banks[i]).Error
		// if err != nil {
		// 	log.Fatalf("cannot seed banks table: %v", err)
		// }
		// users[i].BankID = banks[i].ID

		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
		balance[i].UserID = users[i].ID

		err = db.Debug().Model(&models.UserBalance{}).Create(&balance[i]).Error
		if err != nil {
			log.Fatalf("cannot seed user_balance table: %v", err)
		}
	}
}
