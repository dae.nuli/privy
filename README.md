# privy


## Installation

Clone the repository.

```bash
https://gitlab.com/dae.nuli/privy
```

Switch to the repository folder.

```bash
cd privy
```

Install all dependencies inside go.mod and then run main.go 

```bash
go run main.go
```

Migration and seed will automatically execute after run above command


## API Documentation

Import [API Documentation](PrivyBackendTest.postman_collection.json) on postman to see the documentation